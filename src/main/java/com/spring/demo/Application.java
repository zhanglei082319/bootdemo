package com.spring.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(scanBasePackages = "com.spring.demo")
@RestController
@RequestMapping("/api/v1")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }


    @GetMapping("/test")
    public String add(){
        return "欢迎使用 springboot！";
    }


    @GetMapping
    public String test(){
        return "欢迎使用 , 我爱你中国！";
    }
}
